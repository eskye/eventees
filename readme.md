# Ulearn Community

Building next generation leaders with high technical skills, passionate minds and invest in human development.

# About Ulearn Community

It is a platform that bring learners and instructor together in a classroom

# Getting Started

1. clone the repo on your local drive.
2. cd(change directory) into the the project folder. i.e ulearn.
3. Run `composer install` (on your terminal) to install the vendor files.
4. Create a `.env` file at the root of ulearn folder.
5. copy the cotent of `.env.example` file into `.env` file
6. Run `php artisan serve` (on your terminal) to run the project.
7. When you make changes to the project, push to remote repo.

Clone the repo

```
git clone CLONE-URL
```

Change into the repo

```
composer install
```

Start the project

```
php artisan serve
```

Visit `http://localhost:8000/` to view the project.

# Seeding Your Database

Run your localserver either wamp/xampp
Create a database. update your `.env` file with the database name, username and password(if it exist)

After cd(change directory) into the the project folder. i.e ulearn
Run

```
php artisan db:seed
```

OR Shorthand with migration. This will rollback the migrations and insert fresh data automatically into it

```
php artisan migrate:refresh --seed
```

**NOTE:**

&mdash; The API is a version API. To access API specified url:

Visit `http://localhost:8000/api/v1/` e.g

To access list of levels visit:
`http://localhost:8000/api/v1/level/listlevel`

Read more about [Laravel Lumen](https://lumen.laravel.com/docs/5.8)

**Trouble Shooting**

Incase project failed to run after `composer install`.
comment this section out in `serviceProvider.php`

```
         if (is_array($this->app->config['view']['paths'])) {
             foreach ($this->app->config['view']['paths'] as $viewPath) {
                 if (is_dir($appPath = $viewPath.'/vendor/'.$namespace)) {
                     $this->app['view']->addNamespace($namespace, $appPath);
                 }
             }
         }
```
