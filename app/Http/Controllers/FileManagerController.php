<?php

namespace App\Http\Controllers;
use App\Utilities\Utility;
use App\Utilities\FileService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class FileManagerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function uploadImage(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
            'image' => 'required|image|mimes:jpeg,jpg,png,gif|required|max:2048',
            ]
        );
        if($validator->fails()) {
            $this->response['data'] =  Utility::validationErrorFormat($validator->errors()->all());
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }
        try{
            $file = $request->File('image');
            $fileRealPath = $file->getRealPath();
            $filename = time().'.'.$file->getClientOriginalExtension();
            $folderPath = './uploads/speakers';
            if(!is_dir($folderPath)) {
                mkdir($folderPath, 0777);
            }
            $dest = $folderPath.'/'.$filename;
            FileService::uploadAndSaveImage($fileRealPath, $dest);
            $this->response['filename'] = $filename;
            $this->response['isError'] = false;
            return $this->Ok($this->response);
        }catch (\Exception $e){
            $this->response['data'] = 'An error occurred while uploading file';
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }


    }



}
