<?php

namespace App\Http\Controllers;
use App\Utilities\Utility;
use App\Utilities\FileService;
use App\Utilities\MailService;
use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\QrCode;
use Carbon\Carbon;
use Endroid\QrCode\Response\QrCodeResponse;
use PDF;
class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $url = \base_path().'/uploads/budgets/URHTPRnbTh.pdf';
        return response()->json(['index' => 'Hello world', 'file'=>$url], 200);
    }

    /*  public function pdf(){
        define('BUDGETS_DIR', 'uploads/budgets'); // I define this in a constants.php file

    if (!is_dir(BUDGETS_DIR)){
    mkdir(BUDGETS_DIR, 0755, true);
     }

     $outputName = str_random(10); // str_random is a [Laravel helper](http://laravel.com/docs/helpers#strings)
        $pdfPath = BUDGETS_DIR.'/'.$outputName.'.pdf';
        $html = '<html><body>'
        . '<p>Put your html here, or generate it with your favourite '
        . 'templating system.</p>'
        . '</body></html>';
       // File::put($pdfPath, PDF::load($html, 'A4', 'portrait')->output());
        $url = Storage::disk('public')->put($pdfPath, PDF::load($html, 'A4', 'portrait')->output());
        return response()->json(['url'=>$url]);
    } */


    public function pdfs()
    {
        //$pdf->loadHTML('<h1>Test</h1>');
        //return $pdf->stream();
        $data = ['title' => 'Welcome to HDTuto.com'];
        $html = '<html><body>'
        . '<p>Put your html here, or generate it with your favourite '
        . 'templating system.</p>'
        .'<img src="'.$this->qrcodeUri().'"/>'
        . '</body></html>';
        $pdf = PDF::loadHTML($html);
        $pdf = PDF::loadView('welcome', $data)->save(base_path().'/public/uploads/my.pdf');

        // $pdf->stream();
        $stringPdf = \file_get_contents(base_path().'/public/uploads/my.pdf');

        $converted = \base64_encode($stringPdf);
        return response()->json(['res'=>$converted]);
    }

    public function pdf()
    {
        $data = [
        'title' => 'Welcome to HDTuto.com',
        'today' => Carbon::now()->addHour(1)->toDateTimeString(),
         'qrcode'=> FileService::qrcodeUri()
         ];
        $result = FileService::pdfBase64($data, 'pdf.receipt');

        return response()->json(['res'=>$result]);
    }

    public function pdfMail()
    {
        $data = [
        'title' => 'Welcome to HDTuto.com',
        'today' => Carbon::now()->addHour(1)->toDateTimeString(),
         'qrcode'=> FileService::qrcodeUri()
         ];
        $result = FileService::pdfBase64($data, 'pdf.receipt');
        $content = array(
        'subject' => 'Testing Receipt with Attachment',
        'title' => $data['title'],
        'pdf' => $result,
        'today' =>Carbon::now()->addHour(1)->toDateTimeString(),
        'qrcode' => $data['qrcode']
        );
        $response = MailService::sendReceiptWithAttachment($content);
        return response()->json(['res'=>$response], 200);
    }


    public function qrCode()
    {
        $qrCode = new QrCode('Life is too short to be generating QR codes');
        $qrCode->setSize(300);

        // Set advanced options
        $qrCode->setWriterByName('png');
        $qrCode->setMargin(10);
        $qrCode->setEncoding('UTF-8');
        $qrCode->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH());
        $qrCode->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
        $qrCode->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
        $qrCode->setLogoSize(150, 200);
        $qrCode->setRoundBlockSize(true);
        $qrCode->setValidateResult(false);
        $qrCode->setWriterOptions(['exclude_xml_declaration' => true]);

        // Directly output the QR code
        header('Content-Type: '.$qrCode->getContentType());
        echo $qrCode->writeString();

        // Save it to a file
        $qrCode->writeFile(base_path('public/uploads').'/qrcode.png');

        // Create a response object
        $response = new QrCodeResponse($qrCode);
        return response(['res'=>$response], 200);

    }

    public static function qrcodeUri($data = null)
    {

        $token = Utility::genToken();
         $qrCode = new QrCode($token.'#'.'Sunkanmi Ijatuyi');

          $qrCode->setSize(300);
         $qrCode->setEncoding('UTF-8');
         $qrCode->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH());
         $qrCode->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
         $qrCode->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
         $qrCode->setLabelFontSize(20);
         $data['base64Qr']= $qrCode->writeDataUri();
         //return $qrCode->writeDataUri();
         return response(['res'=>$data], 200);
    }



}
