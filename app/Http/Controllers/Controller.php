<?php

namespace App\Http\Controllers;

use App\Utilities\Constants;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
     public $result = array();
     public $response = array();
    public function Ok(array $response)
    {
        $this->result['data'] = $response['data'];
        $this->result['total'] = $response['total'];
        $this->result['isError'] = $response['isError'];
        return response()->json($this->result, Constants::$OkStatus);
    } 

    public function Bad(array $response, $code = 400)
    {
        return response()->json($response, $code);
    }

    public function Success(array $response)
    {
        return response()->json($response, Constants::$OkStatus);
    }

}
