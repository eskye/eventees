<?php

namespace App\Http\Controllers;

use App\Entities\AuthorizationToken;
use App\Entities\Device;
use App\Helpers\DbHelper;
use App\Utilities\Constants;
use App\Utilities\FileService;
use App\Utilities\Utility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DeviceAuthorizationController extends Controller
{
    //

    public function generateToken()
    {
        try{
            $token = Utility::genToken(Constants::$tokenSize);
            $qrcode = FileService::qrcodeUri($token);
            $generated = AuthorizationToken::create(
                [
                'user_id' => Auth::id(),
                'qrcode' => $qrcode,
                'token' => $token,
                'expires_in' => 60,
                ]
            );
            if($generated) {
                $this->response['data'] = $generated;
                $this->response['isError'] = false;
                return $this->Success($this->response);
            }
            $this->response['data'] = 'An error occurred, could not generate token';
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }catch (\Exception $e){
            $this->response['data'] = 'An error occurred';
            $this->response['isError'] = true;

            return $this->Bad($this->response);
        }
    }

    public function timedOut(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
            'token_id' => 'required|integer',
            'token' => 'required|string'
            ]
        );

        if($validator->fails()) {
            $this->response['data'] =  Utility::validationErrorFormat($validator->errors()->all());
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }
        try{
            $token = AuthorizationToken::where('id', $request->token_id)->where('token', $request->token);
            // $token->hasExpired = true;
            $commit = $token->update(['hasExpired'=>true]);
            if($commit) {
                $this->response['data'] = AuthorizationToken::find($request->token_id);
                $this->response['isError'] = false;
                return $this->Success($this->response);
            }
            $this->response['data'] = 'invalid token';
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }catch (\Exception $e){
            $this->response['data'] = 'An error occurred';
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }

    }

    public function regenerateToken(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
            'token_id' => 'required|integer',
            'token' => 'required|string'
            ]
        );
        if($validator->fails()) {
            $this->response['data'] =  Utility::validationErrorFormat($validator->errors()->all());
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }
        $token = Utility::genToken(Constants::$tokenSize);
        $qrcode = FileService::qrcodeUri($token);

        try{
            $tokenObj = AuthorizationToken::where('id', $request->token_id)->where('token', strtoupper($request->token));
            // $token->hasExpired = true;
            $commit = $tokenObj->update(['hasExpired'=>false,'qrcode'=>$qrcode,'token' => $token]);
            if($commit) {
                $this->response['data'] =  AuthorizationToken::find($request->token_id);;
                $this->response['isError'] = false;
                return $this->Success($this->response);
            }
            $this->response['data'] = 'Invalid token, unable to generate token';
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }catch (\Exception $e) {
            $this->response['data'] = 'An error occurred';
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }
    }

    public function authorizeDevice(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
            'token' => 'required|string',
            'devicename' => 'required|string',
            'imel' =>'required|string',
            'user' => 'required|integer'
            ]
        );
        if($validator->fails()) {
            $this->response['data'] =  Utility::validationErrorFormat($validator->errors()->all());
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }
        try{
            $token = AuthorizationToken::where('token', $request->token)->where('user_id', $request->user)->where('hasExpired', false)->first();
            if(!$token) {
                $this->response['data'] = 'Invalid token, unable to generate token';
                $this->response['isError'] = true;
                return $this->Bad($this->response);
            }else{
                //Check if device has already been authorized
                $device = Device::where('imel', $request->imel)->where('user_id', $request->user)->where('isAuthorized', true)->first();
                if($device) {
                    $this->response['data'] = 'Device has been authorized already';
                    $this->response['isError'] = true;
                    return $this->Bad($this->response);
                }
                $authorized = Device::create(
                    [
                    'user_id' => Auth::id(),
                    'imel' => $request->imel,
                    'name' => $request->devicename,
                    'authorization_id' => $token->id,
                    'isAuthorized'=> true
                    ]
                );
                if($authorized) {
                    $this->response['data'] = $authorized;
                    $this->response['isError'] = false;
                    return $this->Success($this->response);
                }
                $this->response['data'] = 'An error occurred, could not authorized device';
                $this->response['isError'] = true;
                return $this->Bad($this->response);
            }
        }catch (\Exception $e){
            $this->response['data'] = 'An error occurred';
            $this->response['isError'] = true;

            return $this->Bad($this->response);
        }

    }

    public function revokeDevice(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
            'imel' => 'required|string',
            'user' => 'required|integer'
            ]
        );
        if($validator->fails()) {
            $this->response['data'] = Utility::validationErrorFormat($validator->errors()->all());
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }
        try{
            $query = DbHelper::DbQuery('devices')->where('imel', $request->imel)->where('user_id', $request->user);
            $deviceObj = $query->first();
            $tokenObj = AuthorizationToken::findOrFail($deviceObj->authorization_id);
            if($tokenObj) {$tokenObj->delete();
            }
            $device = $query->delete();
            if($device) {
                $this->response['data'] = 'Device has been revoked';
                $this->response['isError'] = false;
                return $this->Success($this->response);
            }
            $this->response['data'] = 'Device does not exist';
            $this->response['isError'] = true;
            return $this->Bad($this->response, 404);

        }catch (\Exception $e){
            $this->response['data'] = 'Not found';
            $this->response['isError'] = true;

            return $this->Bad($this->response, 404);
        }
    }

    public function revokeAllDevices(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
            'user' => 'required|integer'
            ]
        );
        if($validator->fails()) {
            $this->response['data'] =  Utility::validationErrorFormat($validator->errors()->all());
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }
        try{
            $query = DbHelper::DbQuery('devices')->where('user_id', $request->user);
            $deviceObjs = $query->get();
            foreach ($deviceObjs as $deviceObj){
                $tokenObj = AuthorizationToken::findOrFail($deviceObj->authorization_id);
                if($tokenObj) {$tokenObj->delete();
                }
            }
            $revoked = $query->delete();
            if($revoked) {
                $this->response['data'] = 'All connected devices has been revoked';
                $this->response['isError'] = false;
                return $this->Success($this->response);
            }
            $this->response['data'] = 'Device does not exist';
            $this->response['isError'] = true;
            return $this->Bad($this->response);

        }catch (\Exception $e){
            $this->response['data'] = 'An error occurred';
            $this->response['isError'] = true;

            return $this->Bad($this->response);
        }


    }


}
