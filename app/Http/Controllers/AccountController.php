<?php

namespace App\Http\Controllers;



use App\Mail\AccountActivation;
use App\Utilities\MailService;
use App\Utilities\Utility;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Repositories\RoleRepositoryEloquent as Role;
use App\Repositories\UserRepositoryEloquent as User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;


class AccountController extends Controller
{
    //
    protected $role;
    protected $user;
    public $response= array();

    public function __construct(Role $roleRepository, User $userRepository)
    {
        $this->role = $roleRepository;
        $this->user = $userRepository;
    }

    public function createRole(Request $request)
    {

         $this->role->createRoles($request->rolename);
         $this->response['data'] = 'Created successfully';
         $this->response['isError'] = false;
        return $this->Success($this->response);
    }

    public function getRoles()
    {
        $roles = $this->role->getRoles();
        return response()->json(['roles' => $roles], 200);
    }

    public function getUser(Request $request)
    {

        $role = $request->get('role');
        $status = $request->get('status');
        $name = $request->get('name');
        $counte = $request->get('count');
        $period = $request->get('period');
        $count = empty($counte) || $counte == 'undefined' ? 10 : $counte;
        // $output = $this->user->with('role')->paginate(10,['name','email','role_id','phone','isConfirmed','isActive','created_at']);
        $output = $this->user->GetUserList($count, $role, $period, $status, $name);
        // return $output;
        $this->response['items'] = [];
        foreach ($output['output'] as $item){
            $params = [
                'name' => $item->name,
                'email' => $item->email,
                'phone' => $item->phone,
                'created_at' => $item->created_at,
                'isActive' => $item->isActive,
                'rolename' => $item->rolename,
                'serialkey' => $item->activationcode
            ];
            array_push($this->response['items'], $params);
        }
        // $this->result['links'] = $output;
        $output['isError'] = false;
        $output['data'] = $this->response['items'];
        return $this->Ok($output);
    }



    public function createUser(Request $request)
    {


        $validator = Validator::make(
            $request->all(), [
            'email' => 'required|string|email|max:50|unique:users',
            'password' => 'required',
            'phone' => 'required',
            'name' => 'required',
            'role' => 'required'

            ]
        );
        if($validator->fails()) {
            $this->response['data'] =  Utility::validationErrorFormat($validator->errors()->all());
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }

        $role = $request->role;
        $roleId = $this->role->GetRoleByName($role);
        if ($roleId) {

            $activationcode = mt_rand(100000, 300000);
            $requests = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'phone' => $request->phone,
                'role_id' => $roleId->id,
                'activationcode' => $activationcode
            ];
            // dd($request->input());
            //        // After creating the user send an email with the random token generated in the create method above

            $user = $this->user->create($requests);
            $content = array(
                'subject' => 'Account activation mail',
                'code' => $user->activationcode,
                'link' => 'http://localhost:4200/auth/activate?c='.\hash('SHA512', $user->activationcode)
            );
            $res = MailService::activateAccount($content, $user);
            //            if($res['status'] == false){
            //                //Retry sending it again
            //                MailService::sendSingleActivationMail($user);
            //            }
            $response['data'] = $user;
            $response['isError'] = false;
            return response()->json($response);
        }

        $response['error']="TRUE";
        $response['message']='Oops!, an error occurred';
        return response()->json(['error'=>$response], 400);
    }


    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postLogin(Request $request)
    {

        try {
            $validator = Validator::make(
                $request->all(), [
                'email' => 'required|email|max:255',
                'password' => 'required',
                ]
            );
            if($validator->fails()) {
                $this->response['data'] =  Utility::validationErrorFormat($validator->errors()->all());
                $this->response['isError'] = true;
                return $this->Bad($this->response);
            }
        } catch (ValidationException $e) {
            return $e->getResponse();
        }

        try {

             $activate = $this->user->findByField('email', $request->email, ['isActive'])->first();
            if(!$activate->isActive) { return response()->json(['error'=>'Account not active'], 400);
            }

            // Attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt(
                $this->getCredentials($request)
            )
            ) {
                return $this->onUnauthorized();
            }
        } catch (JWTException $e) {
            // Something went wrong whilst attempting to encode the token
            return $this->onJwtGenerationError();
        }

        // All good so return the token
        return $this->onAuthorized($token);
    }


    public function AccountActivation(Request $request)
    {
        try{
            $validator = Validator::make(
                $request->all(), [
                'code' => 'required|string',
                ]
            );
            if($validator->fails()) {
                $this->response['data'] =  Utility::validationErrorFormat($validator->errors()->all());
                $this->response['isError'] = true;
                return $this->Bad($this->response);
            }

        }catch (ValidationException $e){
            return $e->getResponse();
        }

        try{
            $user = $this->user->findByField('activationcode', $request->code, ['*'])->first();

            if($user) {
                $input = array();
                $input['isActive']=true;
                $input['isConfirmed']=true;
                $input['isExpired']=true;
                $input['date_confirmed']=Carbon::now()->addHour(1)->toDateTimeString();
                // MailService::sendSingleActivationMail($user);
                 $this->user->hidden(['id','role_id'])->update($input, $user->id);
                // $this->sendMail();
                $this->response['data'] = 'Account has been activated successfully';
                $this->response['isError'] = true;
                return $this->Success($this->response);

            }
        }catch (\Exception $e){
            $this->response['data'] = 'Account does not exist';
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }

        $this->response['data'] = 'Account does not exist';
        $this->response['isError'] = true;
        return $this->Bad($this->response);
    }




    /**
     * What response should be returned on invalid credentials.
     *
     * @return JsonResponse
     */
    protected function onUnauthorized()
    {
        return new JsonResponse(
            [
            'message' => 'Wrong login details'
            ], Response::HTTP_BAD_REQUEST
        );
    }

    /**
     * What response should be returned on error while generate JWT.
     *
     * @return JsonResponse
     */
    protected function onJwtGenerationError()
    {
        return new JsonResponse(
            [
            'message' => 'could_not_create_token'
            ], Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }

    /**
     * What response should be returned on authorized.
     *
     * @return JsonResponse
     */
    protected function onAuthorized($token)
    {
        $role = JWTAuth::user()->role_id;
        $rolename = $this->role->findByField('id', $role, ['rolename' => 'rolename'])->first();
        return new JsonResponse(
            [
            'message' => 'token_generated',
            'data' => [
                'token' => $token,
                'token_type' => 'bearer',
                'role'=> $rolename->rolename,
                 'user'=>JWTAuth::user(),
                'expires_in'=>JWTAuth::factory()->getTTL() * 60 * 24 * 14

            ]
            ]
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    /**
     * Invalidate a token.
     *
     * @return JsonResponse|Response
     */
    public function deleteInvalidate()
    {
        $token = JWTAuth::parseToken();

        $token->invalidate();

        return new JsonResponse(['message' => 'token_invalidated']);
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse|Response
     */
    public function patchRefresh()
    {
        $token = JWTAuth::parseToken();

        $newToken = $token->refresh();

        return new JsonResponse(
            [
            'message' => 'token_refreshed',
            'data' => [
                'token' => $newToken
            ]
            ]
        );
    }

    /**
     * Get authenticated user.
     *
     * @return JsonResponse|Response
     */
    public function getUsers()
    {
        return new JsonResponse(
            [
            'message' => 'authenticated_user',
            'data' => JWTAuth::parseToken()->authenticate()
            ]
        );
    }


    public function sendMailFirst()
    {
        $users = $this->user->findWhere(['isActive' => 0]);
        foreach ($users as $eachUser) {
            Mail::send(
                'emails.account-activate', ['user' => $eachUser], function ($mail) use ($eachUser) {
                    $mail->from('no-reply@ulearncommunity.com');
                    $mail->subject('Activation of Account');
                    $mail->to($eachUser['email'], $eachUser['name']);
                }
            );
            //return $users;
        }
    }

    public function sendMail()
    {
        try{
              $users = $this->user->findWhere(['isActive'=>0]);

            foreach ($users as $eachUser){

                $content = [
                    'subject' => 'Ulearn Community Account Activation',
                    'body' => 'Dear'.$eachUser->name.'<br/> <p style="font-size: 13px; font-weight: 600">Thank you for creating an account with us. <br/>We need you to confirm and activate your account.</p>',
                    'Footer' => '--Ulearn Community Team | reach us at http://www.ulearncommunity.com',
                    'button' => 'Activate',
                ];
                $email_code = $eachUser['activationcode']; //substitute this line with the activate code store inside the database
                Mail::to($eachUser)->send(new AccountActivation($content, $email_code, $eachUser['email']));
            }
              $response = array(
                  'status' => 'success',
                  'data' => 'Email sent Successfully',
              );
              return response()->json($response, 200);
        }
        catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage(),'error-trace'=>$e->getTrace()], 500);
        }

    }

}






