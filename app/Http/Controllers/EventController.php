<?php

namespace App\Http\Controllers;

use App\Entities\Speakers;
use App\Entities\Ticket;
use App\Utilities\FileService;
use App\Utilities\Utility;
use Illuminate\Http\Request;
use App\Repositories\EventRepositoryEloquent as Event;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Image;
class EventController extends Controller
{
    //
    protected $event;

    public function __construct(Event $eventRepositoryEloquentt)
    {
        $this->event = $eventRepositoryEloquentt;
    }

    public function getEvents(Request $request)
    {
        $role = $request->get('role');
        $status = $request->get('status');
        $name = $request->get('name');
        $counte = $request->get('count');
        $period = $request->get('period');
        $count = empty($counte) || $counte == 'undefined' ? 10 : $counte;
        // $output = $this->user->with('role')->paginate(10,['name','email','role_id','phone','isConfirmed','isActive','created_at']);
        $output = $this->event->GetEventList($count, $role, $period, $status, $name);
        // return $output;
        $this->response['items'] = [];
        foreach ($output['output'] as $item){
            $params = [
                'name' => $item->name,
                'email' => $item->email,
                'phone' => $item->phone,
                'created_at' => $item->created_at,
                'isActive' => $item->isActive,
                'rolename' => $item->rolename,
                'serialkey' => $item->activationcode
            ];
            array_push($this->response['items'], $params);
        }
        // $this->result['links'] = $output;
        $output['isError'] = false;
        $output['data'] = $this->response['items'];
        return $this->Ok($output);
    }

    public function getEventById($eventId)
    {
        $event = $this->event->GetEventById($eventId);
        $this->response['data'] = $event;
        $this->response['isError'] = false;
        return $this->Success($this->response);

    }

    public function createEvent(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
            'name' => 'required|string',
            'description' => 'required|string',
            'location' => 'required|string',
            'startAt' => 'required',
            'endAt' => 'required',
            'cover_image' => 'required|image|mimes:jpeg,jpg,png,gif|required|max:2048',
            'latitude' => 'required',
            'longitude' => 'required',
            'category_id' => 'required|integer',
            'isPublish' => 'required'
            ]
        );

        if($validator->fails()) {
            $this->response['data'] =  Utility::validationErrorFormat($validator->errors()->all());
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }

        $file = $request->File('cover_image');
        $fileRealPath = $file->getRealPath();
        $filename = time().'.'.$file->getClientOriginalExtension();
        $dest = './uploads'.'/'.$filename;
        FileService::uploadAndSaveImage($fileRealPath, $dest);
        $input = $request->all();
        $input['user_id'] = Auth::id();
        $input['cover_image'] = $filename;
        $save = $this->event->create($input);
        if($save) {

            //get last created event Id
            //Check if Ticket is available,
            //Check if Speaker is available,
            $this->response['data'] = 'Event Created Successfully '. $save->id;
            $this->response['isError'] = false;
            return $this->Success($this->response);
        }
        $this->response['data'] = 'An error occurred, Could not create event';
        $this->response['isError'] = true;

        return $this->Bad($this->response);
    }

    public function createTickets(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
            'tickets' => 'required|array',
            'eventId' => 'required|integer'
            ]
        );
        if($validator->fails()) {
            $this->response['data'] =  Utility::validationErrorFormat($validator->errors()->all());
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }
        $tickets = $request->tickets;
        try{
            if(count($tickets) > 0) {
                foreach ($tickets as $ticket){
                    Ticket::create(
                        [
                        'name' => $ticket->name,
                        'description' => $ticket->description,
                        'isPaid' => $ticket->isPaid,
                        'quantity' => intval($ticket->quantity),
                        'isVisible' => $ticket->isVisible,
                        'saleStart' => $ticket->saleStart,
                        'saleEnd' => $ticket->saleEnd,
                        'max_order' => $ticket->maxOrder,
                        'event_id' => $request->eventId
                        ]
                    );
                }
                $this->response['data'] = 'Tickets created successfully';
                $this->response['isError'] = false;
                return $this->Ok($this->response);
            }
            $this->response['data'] = 'Empty record parsed';
            $this->response['isError'] = true;
            return $this->Bad($this->response);

        }catch (\Exception $e){
            $this->response['data'] = 'An error occurred';
            $this->response['isError'] = true;

            return $this->Bad($this->response);
        }
    }

    public function createSpeakers(Request $request)
    {
        $validator = Validator::make(
            $request->all(), [
            'speakers' => 'required|array',
            'eventId' => 'required|integer'
            ]
        );
        if($validator->fails()) {
            $this->response['data'] =  Utility::validationErrorFormat($validator->errors()->all());
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }
        $speakers = $request->speakers;
        try{
            if(count($speakers) > 0) {
                foreach ($speakers as $speaker){
                    Speakers::create(
                        [
                        'name' => $speaker->name,
                        'description' => $speaker->description,
                        'linkedin_url' => $speaker->linkedin,
                        'facebook_url' => $speaker->facebook,
                        'twitter_url' => $speaker->twitter,
                        'photo' => $speaker->photoUrl,
                        'event_id' => $request->eventId
                        ]
                    );
                }
                $this->response['data'] = 'Speakers created successfully';
                $this->response['isError'] = false;
                return $this->Ok($this->response);
            }
            $this->response['data'] = 'Empty record parsed';
            $this->response['isError'] = true;
            return $this->Bad($this->response);

        }catch (\Exception $e){
            $this->response['data'] = 'An error occurred';
            $this->response['isError'] = true;

            return $this->Bad($this->response);
        }
    }


}
