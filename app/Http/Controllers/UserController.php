<?php

namespace App\Http\Controllers;

use App\Utilities\Utility;
use Illuminate\Http\Request;
use App\Repositories\RoleRepositoryEloquent as Role;
use App\Repositories\UserRepositoryEloquent as User;
use Illuminate\Support\Facades\Auth; 

class UserController extends Controller
{
    //
    protected $user;
    public $response= array();

    public function __construct(Role $roleRepository, User $userRepository)
    {
        $this->role = $roleRepository;
        $this->user = $userRepository;
    }

    public function getUserById()
    {
        $user = $this->user->GetUserById(Auth::user()->id);
        $this->response['data'] = $user;
        $this->response['isError'] = false;
        return $this->Success($this->response);
        
    }


    public function editUser(Request $request)
    {
        $validator = Utility::validate(
            $request, [
            'gender' => 'required',
            'address' => 'required',
            'specialty' => 'required',
            'state' => 'required',
            'key' => 'required|string'
            ]
        );
        if ($validator) {
            try {
                $user = $this->user->GetUserById(Auth::id());
                //Do check if account is activated or not
                if (!$user->isActive || !$user->isConfirmed) {
                    $response['error'] = "TRUE";
                    $response['message'] = 'Your account have not been activated';
                    return response()->json(['error' => $response], 400);
                }
                $input = [
                    'phone' => $user->phone,
                    'name' => $user->name,
                    'user_id' => $user->id,
                    'gender' => $request->gender,
                    'specialty' => $request->specialty,
                    'address' => $request->address,
                    'state' => $request->state
                ];

                $this->user->update($input, $user->id);
                return response()->json(['success'=>true], 200);

            } catch (\Exception $e) {

                return response()->json(['error' =>'An error occurred: ln'.$e->getLine()], 400);
            }


        }
        return response()->json($validator, 400);
    }
}
