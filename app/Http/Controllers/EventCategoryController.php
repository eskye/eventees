<?php

namespace App\Http\Controllers;

use App\Entities\EventCategory;
use App\Helpers\DbHelper;
use App\Utilities\Utility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EventCategoryController extends Controller
{
    //

    public function createCategory(Request $request)
    {
        $validate = Validator::make(
            $request->all(), [
            'name' => 'required|string'
            ]
        );

        if($validate->fails()) {
            $this->response['data'] =  Utility::validationErrorFormat($validate->errors()->all());
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }

        $input = $request->all();
        $save = EventCategory::create($input);
        if($save) {
            $this->response['data'] = 'Event Created Successfully';
            $this->response['isError'] = false;
            return $this->Success($this->response);
        }
        $this->response['data'] = 'An error occurred, Could not create event';
        $this->response['isError'] = true;

        return $this->Bad($this->response);
    }

    public function index()
    {
        $categories = EventCategory::all();
        $this->response['data'] = $categories;
        $this->response['isError'] = false;
        return $this->Success($this->response);
    }

    public function editCategory(Request $request)
    {

        $validate = Validator::make(
            $request->all(), [
            'name' => 'required|string',
            'id' => 'required|integer'
            ]
        );

        if($validate->fails()) {
            $this->response['data'] =  Utility::validationErrorFormat($validate->errors()->all());
            $this->response['isError'] = true;
            return $this->Bad($this->response);
        }
        try{
            $category = EventCategory::findOrFail($request->id);
            $category->name = $request->name;
            $save = $category->save();
            if($save) {
                $this->response['data'] = 'Category Updated Successfully';
                $this->response['isError'] = false;
                return $this->Success($this->response);
            }
        }catch (\Exception $e){
            $this->response['data'] = 'An error occurred, Update failed';
            $this->response['isError'] = true;

            return $this->Bad($this->response);
        }

    }


}
