<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Role.
 *
 * @package namespace App\Entities;
 */
class Event extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name',
        'description',
        'location',
        'startAt',
        'endAt',
        'slug',
        'cover_image',
        'latitude','longitude','category_id','user_id',
        'isPublish'
    ];
    protected $hidden =['id','created_at','updated_at'];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function eventcategory(){
        return $this->belongsTo(EventCategory::class, 'category_id');
    }

    public function speakers(){
        return $this->hasMany(Speakers::class);
    }

    public function attendees(){
        return $this->hasMany(Attendees::class);
    }

    public function tickets(){
        return $this->hasMany(Ticket::class);
    }

}
