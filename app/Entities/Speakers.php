<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Role.
 *
 * @package namespace App\Entities;
 */
class Speakers extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','twitter_url','linkedin_url','facebook_url','description','photo','event_id'];
    protected $hidden =['id','created_at','updated_at'];



    public function event(){
        return $this->belongsTo(Event::class,'event_id');
    }

}
