<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Role.
 *
 * @package namespace App\Entities;
 */
class Device extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','imel','isAuthorized','user_id', 'authorization_id'];
    protected $hidden =['id','created_at','updated_at'];



    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function authorizationToken(){
        return $this->belongsTo(AuthorizationToken::class, 'authorization_id');
    }

}
