<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;


/**
 * Class User.
 *
 * @package namespace App\Entities;
 */
class User extends Model implements Transformable, JWTSubject, AuthenticatableContract, AuthorizableContract
{
    use TransformableTrait, Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'email', 'role_id','activationcode',
        'phone','password','name',
        'isActive','isConfirmed',
        'isExpired','date_confirmed'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    
    public function role(){
        return $this->belongsTo(Role::class,'role_id');
    }

    public function events(){
        return $this->hasMany(Event::class);
    }

    public function devices(){
        return $this->hasMany(Device::class);
    }

    public function authorizationTokens(){
        return $this->hasMany(AuthorizationToken::class);
    }

    
}

