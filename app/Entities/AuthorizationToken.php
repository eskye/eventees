<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Role.
 *
 * @package namespace App\Entities;
 */
class AuthorizationToken extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','qrcode','token','expires_in'];
    protected $hidden =['created_at','updated_at'];



    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function devices(){
        return $this->hasMany(Device::class);
    }

}
