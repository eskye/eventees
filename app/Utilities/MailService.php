<?php
/**
 * Created by PhpStorm.
 * User: Olasunkanmi
 * Date: 7/11/2019
 * Time: 3:10 AM
 */

namespace App\Utilities;


use App\Mail\AccountActivation;
use App\Mail\Receipt;

use Illuminate\Support\Facades\Mail;


class MailService
{
  public static $response = array();
    public static function sendSingleActivationMail($user)
    {

        try{
            $content = [
                'subject' => 'Ulearn Community Account Activation',
                'body' => 'Dear'.$user->name.'<br/> <p style="font-size: 13px; font-weight: 600">Thank you for creating an account with us. <br/>We need you to confirm and activate your account.</p>',
                'Footer' => '--Ulearn Community Team | reach us at http://www.ulearncommunity.com',
                'button' => 'Activate',
            ];
            $email_code = $user['activationcode']; //substitute this line with the activate code store inside the database
            Mail::to($user)->send(new AccountActivation($content, $email_code, $user->email));
             self::$response = [ 'status' => true,'data' => 'Email sent Successfully' ];
            return self::$response;
        }
        catch (\Exception $e){
            self::$response = [
                'status' => false,
                'data'=>'Fail to send mail',
                'error' =>$e->getMessage(),
                'error_trace'=>$e->getTrace()
            ];
            return self::$response;
            //return response()->json(['error'=>$e->getMessage(),'error-trace'=>$e->getTrace()],500);
        }
    }


    public static function sendReceiptWithAttachment(array $content){
        try{

            Mail::to('sunkanmiijatuyi@gmail.com','Test Account')->cc('ijatuyitemi194@gmail.com')->send(new Receipt($content, 'sunkanmiijatuyi@gmail.com'));
            self::$response = [ 'status' => true,'data' => 'Email sent Successfully' ];
            return self::$response;
        }catch(\Exception $e){
            self::$response = [
                'status' => false,
                'data'=>'Fail to send mail',
                'error' =>$e->getMessage(),
                'error_trace'=>$e->getTrace()
            ];
            return self::$response;
        }

    }

    public static function activateAccount(array $content, $user){
        try{

            Mail::to($user)->cc('ijatuyitemi194@gmail.com')->send(new AccountActivation($content,$user->email));
            self::$response = [ 'status' => true,'data' => 'Email sent Successfully' ];
            return self::$response;
        }catch(\Exception $e){

            self::$response = [
                'status' => false,
                'data'=>'Fail to send mail',
                'error' =>$e->getMessage(),
                'error_trace'=>$e->getTrace()
            ];
            return self::$response;
        }

    }
}
