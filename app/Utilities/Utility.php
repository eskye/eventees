<?php

/**
 * Created by PhpStorm.
 * User: SilverEdgePC
 * Date: 9/29/2018
 * Time: 6:36 AM
 */
namespace App\Utilities;

use \Illuminate\Support\Facades\Validator;
use \Illuminate\Http\Request;
 class Utility
{
     public static $characters = 'AdededdeAUedede334wdhj23hdj4532beAGDGAA32UAh43ehwe327xzdfr2fr37x83ehADEfrfr3KUNfrLErAfrJAfSfIrNrUrNIVeeEfRSedeITYedEFed44HeJedKdMe33eede2NdPed23eddRededTededUeedVdeWeXdedeY4937';
   public static function validate(Request $request, array $data){
       if(count($data) > 0 && $request != ""){
           $validator =  Validator::make($request->all(),$data);
           if($validator->fails()){
               return $validator->errors();
           }

           return true;
       }
       return null;
   }

     public static function aes128Encrypt($key, $data) {
         if(16 !== strlen($key)) $key = hash('MD5', $key, true);
         $padding = 16 - (strlen($data) % 16);
         $data .= str_repeat(chr($padding), $padding);
         return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, str_repeat("\0", 16)));
     }

     public static  function aes128Decrypt($key, $data) {
         $data = base64_decode($data);
         if(16 !== strlen($key)) $key = hash('MD5', $key, true);
         $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, str_repeat("\0", 16));
         $padding = ord($data[strlen($data) - 1]);
         return substr($data, 0, -$padding);
     }

     public static function genToken($size = 24){
         $string = '';
         for ($i = 0; $i < $size; $i++) {
             $string .= self::$characters[rand(0, strlen(self::$characters) - 1)];
         }
         return strtoupper(substr(hash('SHA512',$string),0,$size));

     }

     public static function validationErrorFormat(array $errors){
         $response['error'] = array();
         $errorResponse = '';
         foreach ($errors as $error){
             $errorResponse .= $error.'\n';
             //array_push($response['error'], $error);
         }
         return $errorResponse;
     }
}