<?php
/**
 * Created by PhpStorm.
 * User: SilverEdgePC
 * Date: 5/17/2019
 * Time: 3:10 PM
 */

namespace App\Utilities;


class Constants
{
  public static $OkStatus = 200;
  public static $Badtatus = 400;
  public static $NotFoundStatus = 404;
  public static $UnauthorizedStatus = 401;
  public static $ServerErrorStatus = 401;
  public static  $innerResponse = array();
  public static $undefined = 'undefined';
  public static $today = 'today';
  public static $week = 'week';
  public static $month = 'month';
  public static $tokenSize = 12;
}