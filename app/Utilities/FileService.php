<?php
/**
 * Created by PhpStorm.
 * User: Olasunkanmi
 * Date: 7/11/2019
 * Time: 3:10 AM
 */

namespace App\Utilities;

use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\QrCode;
use Image;
use PDF;
class FileService{




public static function pdfBase64(array $data, $view){
    //$pdf->loadHTML('<h1>Test</h1>');
    //return $pdf->stream();
   // $data = ['title' => 'Welcome to HDTuto.com'];
    $html = '<html><body>'
        . '<p>Put your html here, or generate it with your favourite '
        . 'templating system.</p>'
        .'<img src="'.self::qrcodeUri().'"/>'
        . '</body></html>';
   $pdf = PDF::loadHTML($html);
   // echo die(var_dump($data));
    $pdf = PDF::loadView($view, $data)->save(base_path().'/public/uploads/my.pdf');

   // $pdf->stream();
    $stringPdf = \file_get_contents(base_path().'/public/uploads/my.pdf');

    $converted = \base64_encode($stringPdf);

   // return 'data:application/pdf;base64,'.$converted;
   return $converted;
   // return response()->json(['res'=>$converted]);
}

public function pdfView(array $data, $view){

}

public function pdfDownload(array $data, $view){

}

public static function qrcodeUri($data = null){

    $token = Utility::genToken();
     $qrCode = new QrCode($data || $token);

      $qrCode->setSize(300);
     $qrCode->setEncoding('UTF-8');
     $qrCode->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH());
     $qrCode->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
     $qrCode->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
     $qrCode->setLabelFontSize(20);
     //$data['base64Qr']= $qrCode->writeDataUri();
     return $qrCode->writeDataUri();
     //return response(['res'=>$data], 200);
 }

 public static function uploadAndSaveImage($realPath, $filePath){
     $img = Image::make($realPath);
     $img->resize(300,300,function($constraint){
         $constraint->aspectRatio();
     })->save($filePath);
     return true;
 }
}



?>
