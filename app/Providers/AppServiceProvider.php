<?php

namespace App\Providers;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
        // Init mailer
        $this->app->singleton(
            'mailer',
            function ($app) {
                return $app->loadComponent('mail', 'Illuminate\Mail\MailServiceProvider', 'mailer');
            }
        );

// Aliases
        $this->app->alias('mailer', Mailer::class);
        // Enable queues
        $this->app->make('queue');
    }
}
