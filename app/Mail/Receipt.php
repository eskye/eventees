<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Receipt extends Mailable
{
    use Queueable, SerializesModels;

    /** @var string the address to send the email */
    protected $content,$mailaddress;



    /**
     * Create a new message instance.
     *
     * @param string $to_address the address to send the email
     * @param float $winnings the winnings they won
     *
     * @return void
     */
    public function __construct($content, $mailaddress)
    {
        $this->content = $content;
        $this->mailaddress = $mailaddress;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('ijatuyitemi194@gmail.com')

            ->subject($this->content['subject'])
            ->view('emails.receipt')
            ->with(['title' => $this->content['title']])
            ->with(['qrcode' => $this->content['qrcode']])
            ->with(['today' => $this->content['today']])
            ->attachData(\base64_decode($this->content['pdf']), 'receipt.pdf', ['mime'=>'application/pdf']);

    }
}
