<?php

namespace App\Repositories;

use App\Helpers\DbHelper;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RoleRepository;
use App\Entities\Role;
use App\Validators\RoleValidator;

/**
 * Class RoleRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RoleRepositoryEloquent extends BaseRepository implements RoleRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Role::class;
    }

    public function getRoles(){
      return DbHelper::DbQuery('roles')->select(['id','rolename'])->get();
    }

    public function createRoles($rolename)
    {
        // TODO: Implement createRoles() method.

         $this->create(['rolename'=>$rolename]);
    }

    public function GetRoleByName($rolename){
        return $this->findByField('rolename', $rolename, ['id' => 'id'])->first();
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
