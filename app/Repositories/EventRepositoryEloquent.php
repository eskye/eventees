<?php

namespace App\Repositories;
use App\Entities\Event;
use App\Helpers\DbHelper;
use App\Utilities\Constants;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;



/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EventRepositoryEloquent extends BaseRepository implements EventRepository
{

    protected $query;


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Event::class;
    }
//    public function model()
//    {
//        return 'App\Entities\User';
//    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function GetEventList($count, $role, $period, $status, $name)
    {
        $this->query =  DbHelper::DbQueryWithJoin('users','roles','.id','.role_id');
        if(!empty($role) && $role != Constants::$undefined){
            $query = $this->query->where('users.role_id','=',$role);
             $this->responseProvider($query,$count);
        } else if(!empty($name) && $name != Constants::$undefined){
        $query =$this->query->where('users.name','LIKE',"%$name%");
            $this->responseProvider($query,$count);
        }else if(!empty($status) && $status != Constants::$undefined){
            $query =$this->query->where('users.isActive','=',$status);
            $this->responseProvider($query,$count);

        }else if(!empty($period) && $period != Constants::$undefined){
             $query = DbHelper::filterByPeriodWithJoin($period,'users', '.created_at','roles','.id','.role_id');
             $this->responseProvider($query,$count);
        } else{
            $this->responseProvider($this->query->orderByDesc('users.created_at'),$count);
        }
        return Constants::$innerResponse;

    }

    protected function responseProvider($query, $count = 0){

        Constants::$innerResponse['output'] = $query->paginate($count,['*']);
        Constants::$innerResponse['total'] = $query->count();
        return Constants::$innerResponse;
    }
    
    public function GetEventById($id){
       return $this->with(['eventcategory', 'speakers', 'tickets'])->findWhere(['id'=>$id])->first();
    }
}
