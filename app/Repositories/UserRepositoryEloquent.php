<?php

namespace App\Repositories;
use App\Helpers\DbHelper;
use App\Utilities\Constants;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Entities\User;


/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{

    protected $query;


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

//    public function model()
//    {
//        return 'App\Entities\User';
//    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function GetUserList($count, $role, $period, $status, $name)
    {
        $this->query =  DbHelper::DbQueryWithJoin('users','roles','.id','.role_id');
        if(!empty($role) && $role != Constants::$undefined){
            $query = $this->query->where('users.role_id','=',$role);
             $this->responseProvider($query,$count);
        } else if(!empty($name) && $name != Constants::$undefined){
        $query =$this->query->where('users.name','LIKE',"%$name%");
            $this->responseProvider($query,$count);
        }else if(!empty($status) && $status != Constants::$undefined){
            $query =$this->query->where('users.isActive','=',$status);
            $this->responseProvider($query,$count);

        }else if(!empty($period) && $period != Constants::$undefined){
             $query = DbHelper::filterByPeriodWithJoin($period,'users', '.created_at','roles','.id','.role_id');
             $this->responseProvider($query,$count);
        } else{
            $this->responseProvider($this->query->orderByDesc('users.created_at'),$count);
        }
        return Constants::$innerResponse;

    }

    protected function responseProvider($query, $count = 0){

        Constants::$innerResponse['output'] = $query->paginate($count,['*']);
        Constants::$innerResponse['total'] = $query->count();
        return Constants::$innerResponse;
    }
    
    public function GetUserById($id){
       return $this->with('role')->findWhere(['id'=>$id])->first();
    }
}
