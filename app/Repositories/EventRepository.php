<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository.
 *
 * @package namespace App\Repositories;
 */
interface EventRepository extends RepositoryInterface
{
   public function GetEventList($count, $role,$period, $status, $name);
   public function GetEventById($id);
}
