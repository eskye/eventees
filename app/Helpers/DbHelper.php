<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/12/2019
 * Time: 7:15 PM
 */

namespace App\Helpers;
use App\Utilities\Constants;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DbHelper
{
    // public static $response = array();
     protected static $query;
    public static function DbQuery($tablename){
      return  DB::table($tablename);
    }

    public static function DbQueryWithJoin($tablename, $joinedTablename, $column, $finderColumn){
        return self::DbQuery($tablename)->join($joinedTablename, $joinedTablename.$column,'=',$tablename.$finderColumn);
    }

    public static function filterByPeriod($period,$tablename, $column){
        switch ($period){
            case Constants::$today:
               self::$query = self::DbQuery($tablename)
                   ->whereDay($column,Carbon::today());
                break;
            case Constants::$week:
                self::$query = self::DbQuery($tablename)
                    ->where($column, '>', Carbon::now()->startOfWeek())
                    ->where($column, '<', Carbon::now()->endOfWeek());

                break;
            case Constants::$month:
                self::$query = self::DbQuery($tablename)->whereMonth($column,date('m'));
                break;
            default:
                break;
        }
        return self::$query;
    }
    public static function filterByPeriodWithJoin ($period,$tablename, $filtercolumn,$joinedTablename, $column, $finderColumn)
    {
        switch ($period) {
            case Constants::$today:
                self::$query = self::DbQueryWithJoin($tablename, $joinedTablename, $column, $finderColumn)
                    ->whereDay($tablename . $filtercolumn, Carbon::today());
                break;
            case Constants::$week:
                self::$query = self::DbQueryWithJoin($tablename, $joinedTablename, $column, $finderColumn)
                    ->where($tablename . $filtercolumn, '>', Carbon::now()->startOfWeek())
                    ->where($tablename . $filtercolumn, '<', Carbon::now()->endOfWeek());
                break;
            case Constants::$month:
                self::$query = self::DbQueryWithJoin($tablename, $joinedTablename, $column, $finderColumn)
                    ->whereMonth($tablename . $filtercolumn, date('m'));
                break;
            default:
                break;
        }
        return self::$query;
    }




}