<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id')->autoIncrement();
            $table->string('name',256);
            $table->longText('description');
            $table->string('location');
            $table->dateTime('startAt');
            $table->dateTime('endAt');
            $table->string('slug');
            $table->string('cover_image');
            $table->string('latitude');
            $table->string('longitude');
            $table->bigInteger('category_id')->foreign()->nullable(); 
            $table->bigInteger('user_id')->foreign();
            $table->boolean('isPublish')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
