<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasexpiredAuthorizationTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('authorization_tokens', function (Blueprint $table) {
            //
            $table->boolean('hasExpired')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('authorization_tokens', function (Blueprint $table) {
            //
            $table->dropColumn('hasExpired');
        });
    }
}
