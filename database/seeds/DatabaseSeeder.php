<?php

use App\Entities\Role;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        Role::truncate();

        $roleQuantity = 2;

        factory(Role::class, $roleQuantity)->create();
    }
}
