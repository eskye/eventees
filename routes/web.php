<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$api = app('Dingo\Api\Routing\Router');

$api->version('v1',function ($api) {
     

    $api->group(['namespace'=>'App\Http\Controllers'],function ($router){
        $router->get('/home', 'ExampleController@index'); 
        $router->get('/pdf', 'ExampleController@pdf');
        $router->get('/pdfmail', 'ExampleController@pdfMail');
        $router->get('/qrcodes','ExampleController@qrCode');
        $router->get('/qrcode','ExampleController@qrcodeUri');
    
        $router->group(['prefix'=>'account'], function () use($router){ 
            $router->post('login','AccountController@postLogin');
            $router->patch('refresh',['uses'=>'AccountController@patchRefresh']);
            $router->post('signup','AccountController@createUser');
            $router->post('activate','AccountController@AccountActivation');
        });
         
    });

    $api->group(['namespace'=>'App\Http\Controllers','middleware'=>['auth']],function ($router){
        $router->group(['prefix'=>'role'],function ()use($router){
            $router->get('roles', 'AccountController@getRoles');
        });

        $router->group(['prefix'=>'user'],function ()use($router){
            $router->get('getUserInfo', 'UserController@getUserById');
        });

        $router->group(['prefix'=>'event'],function ()use($router){
            $router->post('create', 'EventController@createEvent');
            $router->post('ticket', 'EventController@createTickets');
            $router->post('speakers', 'EventController@createSpeakers');
            $router->get('detail/{id}', 'EventController@getEventById');
            $router->get('list', 'EventController@getEvents');
        });
        $router->group(['prefix'=>'eventcategory'],function ()use($router){
            $router->post('create', 'EventCategoryController@createCategory');
            $router->put('edit', 'EventCategoryController@editCategory');
            $router->get('index', 'EventCategoryController@index');
        });
        $router->group(['prefix'=>'deviceToken'],function ()use($router){
            $router->get('getToken', 'DeviceAuthorizationController@generateToken');
            $router->put('timedOut', 'DeviceAuthorizationController@timedOut');
            $router->post('regenerateToken', 'DeviceAuthorizationController@regenerateToken');
        });

        $router->group(['prefix'=>'authorizeDevice'],function ()use($router){
            $router->post('authorize', 'DeviceAuthorizationController@authorizeDevice');
            $router->post('revoke', 'DeviceAuthorizationController@revokeDevice');
            $router->post('revokeAll', 'DeviceAuthorizationController@revokeAllDevices');

        });
        $router->group(['prefix'=>'fileManager'],function ()use($router){
            $router->post('uploadFile', 'FileManagerController@uploadImage');
        });


    });
});